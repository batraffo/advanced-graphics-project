#pragma once
#include "Vector3.h"
#include "Shapes3d.h"

namespace mgd {

	class Camera {
	public:

		Scalar focal;

		int pixelDimX, pixelDimY;

		Camera(Scalar f, int sx, int sy) :focal(f), pixelDimX(sx), pixelDimY(sy) {}

		Ray primaryRay(int pixelX, int pixelY) {
			Ray r;
			r.p = Point3(0, 0, 0); // the poinf of viewis by definition on the origin
			Scalar clipX = 2.0f * pixelX / pixelDimX - 1.0f;
			Scalar clipY = -2.0f * pixelY / pixelDimY + 1.0f;

			r.d = Vector3(clipX, clipY, focal).normalized();

			return r;
		}

	};

}
