#pragma once
#include <cmath>
#include "Vector3.h"

#define M_PI       3.14159265358979323846   // pi


namespace mgd {

	class Quaternion {
	public:
		Vector3 im;
		Scalar re;

		Quaternion(Vector3 _im, Scalar _re) :im(_im), re(_re) { }
		Vector3 apply(Vector3 p) const;
		void conjugate() {
			im = -im;
		}
		Quaternion conjugated() const {
			return Quaternion(-im, re);
		}

		Quaternion operator -() const {
			return Quaternion(-im, -re);
		}

		static Quaternion fromVector3(Vector3 v) {
			return Quaternion(v, 0);
		}
		static Quaternion fromAngleAxis(Scalar deg, Vector3 axis) {
			Scalar rad = Scalar(deg * M_PI / 180);
			Scalar s = std::sin(rad / 2);
			Scalar c = std::cos(rad / 2);
			return Quaternion(axis.normalized() * s, c);
		}
		static Quaternion identity() {
			return Quaternion(Vector3(0, 0, 0), 1);
		}

		Quaternion operator*(Scalar b) const {
			return Quaternion(im * b, re * b);
		}

		Quaternion operator/(Scalar b) {
			return Quaternion(im / b, re / b);
		}

		Quaternion operator+(const Quaternion& b) {
			return Quaternion(Vector3(im.x + b.im.x, im.y + b.im.y, im.z + b.im.z), re + b.re);
		}

		Quaternion normalized() {
			return (*this) / norm();
		}

		Scalar norm() const {
			return std::sqrt(im.x * im.x + im.y * im.y + im.z * im.z + re * re);
		}

		Quaternion mix_with(Quaternion b, Scalar k) {
			Quaternion result = Quaternion::identity(); 
			result = (*this) * k + (b * (1 - k));
			result = result.normalized();
			return result;
		}

	};

	inline Quaternion operator * (const Quaternion& a, const Quaternion& b) {
		return Quaternion(
			cross(a.im, b.im) + a.im * b.re + a.re * b.im,
			-dot(a.im, b.im) + a.re * b.re
		);
	}


	/*Quaternion operator+(const Quaternion& a, const Quaternion& b) {
		return Quaternion(Vector3(a.im.x + b.im.x, a.im.y + b.im.y, a.im.z + b.im.z), a.re + b.re);
	}

	Quaternion operator*(const Quaternion& a, Scalar b) {
		return Quaternion(a.im * b, a.re*b);
	}

	Quaternion operator/(const Quaternion& a, Scalar b) {
		return Quaternion(a.im / b, a.re / b);
	}*/

	inline Vector3 Quaternion::apply(Vector3 p) const {
		Quaternion q = fromVector3(p);
		q = (*this) * q * this->conjugated();
		assert(areEqual(q.re, 0));
		return q.im;
	}

	inline bool areEqual(const Quaternion& a, const Quaternion& b) {
		return areEqual(a.im, b.im) && areEqual(a.re, b.re);
	}

	inline bool areEquivalent(const Quaternion& a, const Quaternion& b) {
		return areEqual(a, b) || areEqual(a, -b);
	}


}
