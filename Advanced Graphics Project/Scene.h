#pragma once
#include <vector>
#include <string>
#include <iostream>

#include "Transform.h"
#include "Shapes3d.h"
#include "Camera.h"

#include "RaffoThings.h"

#define NUM_PLAYERS 10

namespace mgd {


	const char* intensityToCstr(Scalar intensity, short& color) {
		switch (int(round(intensity * 8))) {
		case 0:
			color = FOREGROUND_INTENSITY;
			return "  "; // darkest
		case 1: return " '";
		case 2: return " +";
		case 3: return " *";
		case 4: return " #";
		case 5: return "'#";
		case 6: return "+#";
		case 7: return "*#";
		case 8: return "##"; // lightest
		default:
		case 10: return "##";
		}
	}


	const char* lighting(Versor3 normal, short& color) {
		Versor3 lightDir = Versor3(1, 2, -2).normalized();
		// lambertian
		Scalar diffuse = dot(normal, lightDir);
		if (diffuse < 0) diffuse = 0;

		return intensityToCstr(diffuse, color);
	}


	class SphereGameObj {
	public:
		Transform transform;
		bool isPlayer;

		// qui  la descrizione del contenuto: una mesh, un collider, etc TUTTO ESPRESSO IN SPAZIO LOCALE
		// Per adesso, tutti i gameObjects sono... sfere con dei nasi
		Sphere body, nose;

		SphereGameObj() :
			transform(),
			body(Vector3(0, 1, 0), 1),
			nose(Vector3(0, 1, 0.5f),0.7f),
			isPlayer(false)
		{
		}

		//Players
		SphereGameObj(bool isPlayer):
			transform(),
			body(Vector3(0,1,0),1),
			nose(Vector3(0,1,0), 0.5),
			isPlayer(true) 
		{
			body.isAPlayer = true;
		}
	};

	class PlaneGameObj {
	public:
		Transform transform;
		// qui  la descrizione del contenuto: una mesh, un collider, etc TUTTO ESPRESSO IN SPAZIO LOCALE
		// Per adesso, tutti i gameObjects sono... sfere con dei nasi
		Plane plane;

		PlaneGameObj() :
			transform(),
			plane(Point3(0, 0, 0), Versor3(0, 1, 0)) {}
	};

	class Scene {
	public:
		std::vector<SphereGameObj> obj; // a set with SphereGameObj (each with its own transform)
		std::vector<SphereGameObj> players; // a set with Player
		PlaneGameObj plane;
		Plane scenePlane;
		//std::vector<Sphere> toWorld() const;

		void populate(int n) {
			for (int i = 0; i < n; i++) {
				SphereGameObj someoneNew;
				someoneNew.transform.translate = Vector3(randomBetween(-30,30), randomBetween(-4,2), randomBetween(3,15));
				someoneNew.transform.scale = randomBetween(0.1f, 3);
				obj.push_back(someoneNew);
			}

			for (int i = 0; i < NUM_PLAYERS; i++) {
				SphereGameObj player = new SphereGameObj(true);
				player.transform.translate = Vector3::random(14) + Vector3(0, 0, 15);
				player.transform.translate.y = -1;
				players.push_back(player);
			}

			plane.transform.translate = Vector3(0, -2, 0);

		}

		// produces a vector of spheres in world space
		std::vector<Sphere> toWorld()  {
			std::vector<Sphere> res;
			

			for (const SphereGameObj& g : obj) {
				res.push_back(apply(g.transform, g.nose, false));
				res.push_back(apply(g.transform, g.body, false));
			}
			for (const SphereGameObj& gi : players) {
				res.push_back(apply(gi.transform, gi.body,true));
			}
			scenePlane = apply(plane.transform, plane.plane);
			return res;
		}

		std::vector<Sphere> toView(Transform camera) {
			std::vector<Sphere> res;

			for (const SphereGameObj& g : obj) {
				res.push_back(apply(camera.inverse() * g.transform, g.nose, false));
				res.push_back(apply(camera.inverse() * g.transform, g.body, false));
			}
			for (const SphereGameObj& gi : players) {
				res.push_back(apply(camera.inverse() * gi.transform, gi.body, true));
			}
			scenePlane = apply(camera.inverse()*plane.transform, plane.plane);
			return res;

		}

		std::vector<SphereGameObj>& getPlayers() {
			return players;
		}

		void rayCasting(const std::vector<Sphere>& sphereVector, CharWithColor* buffer) const {

			int bufferIndex = 0;
			short color = FOREGROUND_INTENSITY;

			Camera c(1, WIDHT, HEIGHT);

			for (int y = 0; y < c.pixelDimY; y++) {
				for (int x = 0; x < c.pixelDimX; x++) {
					Point3 hitPos;
					Point3 hitNorm;
					Scalar distMax = 1000.0;

					for (const Sphere& s : sphereVector) {
						bool success = rayCast(c.primaryRay(x, y), s, hitPos, hitNorm, distMax);
						if (success) {
							color = s.isAPlayer ? FOREGROUND_BLUE : FOREGROUND_GREEN;
						}
					}

					bool success = rayCast(c.primaryRay(x, y), scenePlane, hitPos, hitNorm, distMax);

					if (success)
						color = 7;

					const char* a = lighting(hitNorm, color);

					buffer[bufferIndex].mySymbol = a[0];
					buffer[bufferIndex].color = color;
					bufferIndex++;
					buffer[bufferIndex].mySymbol = a[1];
					buffer[bufferIndex].color = color;
					bufferIndex++;
				}
				buffer[bufferIndex].mySymbol = '\n';
				bufferIndex++;
			}
			buffer[bufferIndex].mySymbol = '\0';
		}

	};	
	


} // end of namespace mgd

