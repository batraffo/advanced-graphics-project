#pragma once
#include "Quaternion.h"

namespace mgd {

	class Transform {
	public:
		Scalar scale;
		Vector3 translate;
		Quaternion rotate;

		Transform() : scale(1), translate(0, 0, 0), rotate(Quaternion::identity()) {
		}

		Transform(Vector3 v) : scale(1), translate(v), rotate(Quaternion::identity()) {
		}

		Transform(Quaternion q) : scale(1), translate(0, 0, 0), rotate(q) {
		}

		Vector3 transformPoint(const Vector3& p) const {
			return rotate.apply(p * scale) + translate;
		}
		Vector3 transformVersor(const Vector3& p) const {
			return rotate.apply(p);
		}
		Vector3 transformVector(const Vector3& p) const {
			return rotate.apply(p * scale);
		}
		Scalar transformScalar(Scalar p) const {
			return p * scale;
		}

		Transform inverse() const {
			Transform t;
			t.scale = (1 / scale);
			t.rotate = rotate.conjugated();
			t.translate = t.rotate.apply(-translate * t.scale);
			// oppure: t.translarte = t.applyToVector( -translate );
			return t;
		}

		void invert() {
			scale = (1 / scale);
			rotate.conjugate();
			translate = rotate.apply(-translate * scale);
			// oppure: translate = applyToVector(-translate);
		}

		void operator *=(const Transform& t) {
			this->rotate = this->rotate * t.rotate;
			this->scale = this->scale * t.scale;
			this->translate = this->transformVector(t.translate) + this->translate;
		}

		static Transform blend(Transform a, Transform b, Scalar k) {
			Transform t;
			t.translate = a.translate * k + b.translate * (1 - k);
			t.scale = a.scale * k + b.scale * (1 - k);
			t.rotate = a.rotate.mix_with(b.rotate, k);
			return t;
		}

	};




	//  first b then a
	Transform operator * (const Transform& a, const Transform& b) {
		Transform t;
		t.rotate = a.rotate * b.rotate;
		t.scale = a.scale * b.scale;
		t.translate = a.transformVector(b.translate) + a.translate;
		return t;
	}

}
