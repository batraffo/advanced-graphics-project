#include <windows.h>
#include <iostream>
#include <chrono>
#include <cstdlib>
#include <conio.h>
#include <time.h>
#include <cwchar>
#include <cstdint>
#include <thread>

#include "Vector3.h"
#include "Shapes3d.h"
#include "Camera.h"

#include "Quaternion.h"
#include "Transform.h"

#include "Scene.h"

#include "RaffoThings.h"


using namespace mgd;


void unitTestLinearOps() {

	Vector3 a(24, 1, 32), b(12, 3, 54);
	Scalar k = 5.0;
	assert(areEqual(a + b, b + a));
	assert(areEqual((a + b) * k, b * k + a * k));
}

void unitTestProdutcs() {
	Vector3 a(24, -3, 32), b(12, 3, -54), c(10, 13, -43);
	Scalar k = 0.4f;

	assert(isZero(dot(a, cross(a, b))));
	assert(isZero(dot(b, cross(a, b))));

	assert(isZero(cross(a, b) + cross(b, a)));

	assert(areEqual(dot(k * a, b), dot(b, k * a)));
	assert(areEqual(cross(a, b + 4 * c), cross(a, b) + 4 * cross(a, c)));
	assert(areEqual(cross(2 * a, 2 * b), 4 * cross(a, b)));
}

void unitTestRaycasts() {
	Ray r(Point3(0, 0, 0), Vector3(1, 0, 0));
	Sphere s(Point3(5, 0, 0), 3);
	Point3 hitPoint;
	Point3 hitNorm;
	Scalar distMax = 100000;

	bool isHit = rayCast(r, s, hitPoint, hitNorm, distMax);
	assert(isHit);
	assert(areEqual(hitPoint, Point3(2, 0, 0)));
}

void unitTestRaycastPlane() {
	Ray r(Point3(0, 0, 0), Vector3(1, 0, 0));
	Plane p(Point3(10, 0, 0), Vector3(-1, 0, 0));
	Point3 hitPoint;
	Point3 hitNorm;
	Scalar distMax = 100000;

	bool isHit = rayCast(r, p, hitPoint, hitNorm, distMax);
	assert(isHit);
	assert(areEqual(hitPoint, Point3(10, 0, 0)));
}


float currentTime() {
	static float now = 0.0;
	now += 0.005f;
	return now;
	//return std::chrono::system_clock().now();
}




void examplesOfSyntax() {
	Vector3 v(0, 2, 3);

	Vector3 r = v * 4;  //Vector3 r = v.operator *(4);
	r *= 0.25;
	Vector3 w = v + r; //  Vector3 w = v.operator + (r);
	v += w;

	v = r - v;
	v = -w + v;
	Scalar k = dot(v, w);

	v.x = 0.4f;

	Scalar h = v[0];

	// v[6] = 0.3;
}

void unitTestQuaternions() {

	{
		Quaternion rot = Quaternion::fromAngleAxis(180, Vector3(0, 1, 0));
		Vector3 p(0, 0, 1);
		Vector3 q = rot.apply(p);
		assert(areEqual(q, Vector3(0, 0, -1)));
	}

	{
		Quaternion rot = Quaternion::fromAngleAxis(90, Vector3(0, 1, 0));
		Vector3 p(3, 5, 6);
		Vector3 q = rot.apply(p);
		assert(areEqual(q, Vector3(6, 5, -3)));
	}


	Quaternion rot = Quaternion::fromAngleAxis(30, Vector3(1, 1, 3));
	Vector3 p(3, 5, 6), q = p;
	for (int k = 0; k < 12; k++) q = rot.apply(q);
	assert(areEqual(q, p));



	{
		Quaternion q = Quaternion::identity();

		Vector3 randomAxis(4, 3, 1);
		Scalar deg = 90.f;

		Quaternion qRot = Quaternion::fromAngleAxis(deg, randomAxis);

		for (int i = 0; i < 8; i++) {
			q = q * qRot;
		}

		assert(areEquivalent(q, Quaternion::identity()));
		assert(areEqual(q, Quaternion::identity()));
	}


}

void unitTestTransformation() {
	Transform t;
	t.rotate = Quaternion::fromAngleAxis(43.4f, Vector3(1, -3, -2));
	t.translate = Vector3(1, 3, 4);
	t.scale = 5;

	Point3 p(4, 10, -13);

	Point3 q = t.transformPoint(p);
	Point3 r = t.inverse().transformPoint(q);

	assert(areEqual(p, r));

	Transform ti = t;
	ti.invert();
	r = ti.transformPoint(q);
	assert(areEqual(p, r));

	Transform tA;
	tA.rotate = Quaternion::fromAngleAxis(-13.4f, Vector3(13, 4, 0));
	tA.translate = Vector3(0, -1, 01);
	tA.scale = Scalar(0.23);

	Transform tB = t;

	Transform tAB = tA * tB;
	assert(areEqual(
		tAB.transformPoint(p),
		tA.transformPoint(tB.transformPoint(p))
	)
	);


}

void windowsSh1t() {
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //stores the console's current dimensions

	MoveWindow(console, r.left, r.top, 16000, 9000, TRUE);

	// get handle to the console window
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	// retrieve screen buffer info
	CONSOLE_SCREEN_BUFFER_INFO scrBufferInfo;
	GetConsoleScreenBufferInfo(hOut, &scrBufferInfo);

	// current window size
	short winWidth = scrBufferInfo.srWindow.Right - scrBufferInfo.srWindow.Left + 1;
	short winHeight = scrBufferInfo.srWindow.Bottom - scrBufferInfo.srWindow.Top + 1;

	// current screen buffer size
	short scrBufferWidth = scrBufferInfo.dwSize.X;
	short scrBufferHeight = scrBufferInfo.dwSize.Y;

	// to remove the scrollbar, make sure the window height matches the screen buffer height
	COORD newSize;
	newSize.X = scrBufferWidth;
	newSize.Y = winHeight;

	// set the new screen buffer dimensions
	int Status = SetConsoleScreenBufferSize(hOut, newSize);
	if (Status == 0)
	{
		std::cout << "SetConsoleScreenBufferSize() failed! Reason : " << GetLastError() << std::endl;
		exit(Status);
	}

	// print the current screen buffer dimensions
	GetConsoleScreenBufferInfo(hOut, &scrBufferInfo);
	//std::cout << "Screen Buffer Size : " << scrBufferInfo.dwSize.X << " x " << scrBufferInfo.dwSize.Y << std::endl;
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = 0;                   // Width of each character in the font
	cfi.dwFontSize.Y = 10;                  // Height
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	std::wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);

	//std::cout << "Font: Consolas, Size: 24\n";
	HWND hwnd = GetConsoleWindow();
	if (hwnd != NULL) { SetWindowPos(hwnd, 0, 0, 0, 1280, 720, SWP_SHOWWINDOW | SWP_NOMOVE); }
}

void cls()
{
	// Get the Win32 handle representing standard output.
	// This generally only has to be done once, so we make it static.
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD topLeft = { 0, 0 };

	// std::cout uses a buffer to batch writes to the underlying console.
	// We need to flush that to the console because we're circumventing
	// std::cout entirely; after we clear the console, we don't want
	// stale buffered text to randomly be written out.
	std::cout.flush();

	// Figure out the current width and height of the console window
	if (!GetConsoleScreenBufferInfo(hOut, &csbi)) {
		// TODO: Handle failure!
		abort();
	}
	DWORD length = csbi.dwSize.X * csbi.dwSize.Y;

	DWORD written;

	// Flood-fill the console with spaces to clear it
	FillConsoleOutputCharacter(hOut, TEXT(' '), length, topLeft, &written);

	// Reset the attributes of every character to the default.
	// This clears all background colour formatting, if any.
	FillConsoleOutputAttribute(hOut, csbi.wAttributes, length, topLeft, &written);

	// Move the cursor back to the top left for the next sequence of writes
	SetConsoleCursorPosition(hOut, topLeft);
}


void setConsoleColour(unsigned short colour)
{
	static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, colour);
}

void printBuffer(CharWithColor* buffer) {
	short lastColor = buffer[0].color;
	setConsoleColour(buffer[0].color);
	for (int i = 0; i < WIDHT * HEIGHT * 2; i++) {
		if (buffer[i].mySymbol == '\0')
			break;
		if (buffer[i].color != lastColor) {
			setConsoleColour(buffer[i].color);
			std::cout<<std::flush;
			lastColor = buffer[i].color;
		}
		std::cout << buffer[i].mySymbol;
	}
	
}

void animate(const Transform& from, const Transform& to,  Scene& s, CharWithColor* buffer) {
	Transform toShow;
	Scalar k = 0.10f;
	cls();

	for (int i = 0; i < 8; i++) {
		toShow = Transform::blend(to, from, k);
		s.rayCasting(s.toView(toShow), buffer);
		printBuffer(buffer);
		std::this_thread::sleep_for(std::chrono::milliseconds(750));
		k += 0.10f;
		cls();
	}
}

int main() {


	/*unitTestLinearOps();
	unitTestProdutcs();
	unitTestRaycasts();
	unitTestRaycastPlane();

	unitTestQuaternions();
	unitTestTransformation();*/

	std::ios::sync_with_stdio(false);

	CharWithColor buffer[WIDHT * HEIGHT * 2 + (HEIGHT+1)];

	srand(static_cast <unsigned> (time(NULL)));


	windowsSh1t();

	Scene s;
	s.populate(30);
	
	s.rayCasting(s.toWorld(), buffer);
	printBuffer(buffer);

	std::vector<SphereGameObj>& players = s.getPlayers();

	int currentPlayer = 0;
	bool externalVision = true;

	while (1) {
		

		
		int input = _getch();
		cls();


		switch (input)
		{
		case ' ':
			
			if (!externalVision) {
				externalVision = true;
				animate(players[currentPlayer].transform, Transform(),s, buffer);
			}
			s.rayCasting(s.toWorld(), buffer);
			break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '0':			
			if (externalVision) {
				externalVision = false;
				currentPlayer = input - '0';
				animate(Transform(), players[currentPlayer].transform, s, buffer);
			}
			else if (currentPlayer != input - '0') {
				animate(players[currentPlayer].transform, players[input - '0'].transform, s, buffer);
				currentPlayer = input - '0';
			}
			s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			break;

		case 'w':
			players[currentPlayer].transform *= Transform(Vector3(0, 0, 1));
			if(externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;


		case 's':
			players[currentPlayer].transform *= Transform(Vector3(0, 0, -1));
			if (externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;

		case 't':
			players[currentPlayer].transform *= Transform(Vector3(0, 1, 0));
			if (externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;


		case 'g':
			players[currentPlayer].transform *= Transform(Vector3(0, -1, 0));
			if (externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;

		case 'a':
			players[currentPlayer].transform *= Transform(Quaternion::fromAngleAxis(-20,Vector3(0,1,0)));
			if (externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;

		case 'd':
			players[currentPlayer].transform *= Transform(Quaternion::fromAngleAxis(20, Vector3(0, 1, 0)));
			if (externalVision)
				s.rayCasting(s.toWorld(), buffer);
			else {
				s.rayCasting(s.toView(players[currentPlayer].transform), buffer);
			}
			break;

		default:
			break;
		}

		printBuffer(buffer);

	}

	

}
