Youtube link: https://www.youtube.com/watch?v=O38e6K2cpu4







Il progetto di questo corso consiste nel implementare delle nuove funzionalità al progetto scritto in classe.

1     First person view: eseguire il rendering dal punto di vista di uno dei "personaggi" nella scena.
      Come: Implementare la funzione "toView" che prende la trasformazione associata ad una camera e la applica
      (invertita!) a tutti i personaggi. Passare la trasformazione di uno dei personaggi a questa funzione.


2      Interfaccia (per testing): in un ciclo infinito, leggere un tasto, si reagire al tasto letto, e mandare un nuovo rendering.
       L'interfaccia deve essere: SPAZIO: switch da first person e visione esterna.
       Tasti WASD + TG : movimento del personaggio controllato (vedi punto successivo). Rotazione (A- D), sposamento in avanti o indietro (WS) 
       Tasti 0..9 scelta del personaggio corrente (controllato, e che osserva la scena se si è in modalità first person).


3      Semplice controllo di un personaggio: spostare uno arbitrario dei personaggi (il giocatore), ruotandolo su se stesso e spostandolo nel  
       proprio sistema di riferimento

4      Opzionale: effetto volo di camera: (opzionale) provare una piccola animazione che, su pressione dei tasti 0 - 9 oppure SPAZIO,
       interpola la tasformazione della camera dalla situazione iniziale a quella finale, implementando a questo scopo un metodo Blend della
       classe trasform


5      Aggiunta del pavimento: aggiungere alla scena un pavimento (un Plane) che viene anche esso trasformato (in spazio mondo e/o vista) e renderizzato. 
